#!/usr/bin/env python3.6
import sys
from pprint import pprint

import requests


def p(s):
    sys.stdout.write(s)
    sys.stdout.flush()


repositories = []
api = 'https://gitlab.com/api/v4'
headers = {'Private-Token': '<GITLAB PRIVATE TOKEN>'}
list_url = f'{api}/users/mkopta/projects'
while True:
    p('.')
    rv = requests.get(list_url, headers=headers)
    if rv.status_code != 200:
        print(f'Failed to list repositories. {rv.text}')
        sys.exit(1)
    repositories.extend(rv.json())
    if 'next' not in rv.links:
        break
    list_url = rv.links['next']['url']
p('\n')
for repo in repositories:
    p('.')
    rv = requests.put(
        f'{api}/projects/{repo["id"]}',
        headers=headers,
        params={
            'issues_enabled': 'false',
            'visibility': 'public',
            'request_access_enabled': 'true',
            'merge_requests_enabled': 'false',
            'issues_enabled': 'false',
            'jobs_enabled': 'false',
            'lfs_enabled': 'false',
            'wiki_enabled': 'false',
            'snippets_enabled': 'false'
        }
    )
    if rv.status_code != 200:
        print(f'Failed to update repository {repo["name"]}. {rv.text}')
        sys.exit(1)
p('\n')
for repo in repositories:
    print(f'Name: {repo["name"]}')
    print(f'  SSH URL: {repo["ssh_url_to_repo"]}')
    print(f'  Visibility: {repo["visibility"]}')
    print(f'  Request access enabled: {repo["request_access_enabled"]}')
    print(f'  Merge requests enabled: {repo["merge_requests_enabled"]}')
    print(f'  Issues enabled: {repo["issues_enabled"]}')
    print(f'  Jobs enabled: {repo["jobs_enabled"]}')
    print(f'  LFS enabled: {repo["lfs_enabled"]}')
    print(f'  Wiki enabled: {repo["wiki_enabled"]}')
    print(f'  Snippets enabled: {repo["snippets_enabled"]}')
