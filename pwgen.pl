#!/usr/bin/perl
use strict;
use warnings;

sub random_pwd {
	my $length = shift;
	my @chars = (0 .. 9, 'a' .. 'z', 'A' .. 'Z');
	return join '', @chars[ map rand @chars, 0 .. $length ];
}
print random_pwd(6) . "\n";
