#!/bin/bash

PORTS=8

function can_i_haz_root {
    if [ $UID -ne 0 ]; then
        echo "Got root?"
        exit 1
    fi
}

function vmnet_on {
    can_i_haz_root
    brctl addbr br0
    ip a a 172.20.20.1/24 dev br0
    ip l s dev br0 up
    for i in $(seq 0  $((${PORTS}-1))); do
        ip tuntap add dev vnet${i} mode tap user mk group kvm
        brctl addif br0 vnet${i}
        ip l s dev vnet${i} up
    done
    iptables -t nat -A POSTROUTING -s 172.20.20.0/24 ! -d 172.20.20.0/24 -j MASQUERADE
    sysctl -w net.ipv4.conf.all.forwarding=1
}

function vmnet_off {
    can_i_haz_root
    for i in $(seq 0 $((${PORTS}-1))); do
        ip l s dev vnet${i} down
        brctl delif br0 vnet${i}
        ip tuntap del dev vnet${i} mode tap
    done
    ip l s dev br0 down
    ip a f dev br0
    brctl delbr br0
    iptables -t nat -D POSTROUTING -s 172.20.20.0/24 ! -d 172.20.20.0/24 -j MASQUERADE
    sysctl -w net.ipv4.conf.all.forwarding=0
}

function usage {
    echo "Usage: $0 <on|off|status>"
}

function main {
    if [ $# -ne 1 ]; then
        usage
        exit 1
    fi
    case $1 in
        "on") vmnet_on;;
        "off") vmnet_off;;
        "status") 
            if ip l | grep -q vnet0; then
                echo "on"
            else
                echo "off"
            fi
            ;;
        *) usage; exit 1;;
    esac
}

main "$@"
