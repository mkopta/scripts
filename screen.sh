#!/bin/sh

echo "$@"

if [ $# -ne 1 ]; then
    echo "Wrong usage"
    exit 1
fi

function all_off {
    outputs=$(xrandr | grep '^[a-zA-Z]' | cut -d\  -f1 | grep -v Screen)
    for output in $outputs; do
        xrandr --output $output --off &
    done
    wait
}

all_off

if [ $1 = "internal" ]; then
    xrandr --output eDP-1 --primary --auto
elif [ $1 = "external" ]; then
    xrandr --output DP-1 --primary --auto
#elif [ $1 = "hdmi4k" ]; then
#    # 4K on Dell P2416Db via Intel GPU of ThinkPad E130
#    xrandr --newmode "2560x1440_30" 146.25 2560 2680 2944 3328 1440 1443 1448 1468 -hsync +vsync
#    xrandr --newmode "2560x1440_55" 220.812 2560 2608 2640 2720 1440 1443 1448 1478 -hsync -vsync
#    xrandr --addmode HDMI1 "2560x1440_30"
#    xrandr --addmode HDMI1 "2560x1440_55"
#    xrandr --output HDMI1 --primary --mode 2560x1440_30 --dpi 102 --set "Broadcast RGB" "Full"
fi
