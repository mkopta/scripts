#!/usr/bin/env bash

if [ $UID -ne 0 ]; then
	echo "Got root?"
	exit 1
fi

ipt=$(which iptables)

if [ $? -ne 0 ]; then
	echo "No iptables?"
	exit 1
fi

function clear() {
	$ipt -t filter -P INPUT ACCEPT
	$ipt -t filter -P FORWARD ACCEPT
	$ipt -t filter -P OUTPUT ACCEPT
	$ipt -t filter -F
	$ipt -t nat -F
}

function begin() {
	$ipt -A INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT 
	$ipt -A INPUT -p icmp -j ACCEPT 
	$ipt -A INPUT -i lo -j ACCEPT 
}

function allow() {
	$ipt -A INPUT -p tcp -m state --state NEW -m tcp -s $3 --dport $1 -j ACCEPT 
}

function allow_udp() {
	$ipt -A INPUT -p udp -s $3 --dport $1 -j ACCEPT 
}

function redirect() {
	$ipt -t nat -A PREROUTING -p tcp --dport $2 -j REDIRECT --to-port $4
}

function end() {
	$ipt -A INPUT -j REJECT --reject-with icmp-host-prohibited 
	$ipt -A FORWARD -j REJECT --reject-with icmp-host-prohibited 
}

function show() {
	$ipt -L -v -n --line-numbers
}

any="0.0.0.0/0"
vpn="10.0.5.128/27"
ftp=21
ssh=22
http=80
ldap=389
https=443

clear
begin
allow $ssh from $any
allow_udp 1194 from $any
redirect port 999 to 9999
end
show
